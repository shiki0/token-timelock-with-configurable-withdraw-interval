// SPDX-License-Identifier: MIT
pragma solidity ^ 0.8.21;

interface IERC20 {
	function transfer(address to, uint256 value) external returns(bool);
	function transferFrom(address from, address to, uint256 value) external returns(bool);
}

contract timeLock {

	struct details {
		uint lockedAmount;
		uint unlockedAmount;
		uint blockStart;
		uint24 blockStop; //using uint24 to limit lock to ~8 years
		uint partialUnlock;
	}

	mapping(address => mapping(address => details)) public lockedDetals;

	function lock(address token, uint amount, uint16 _days, uint _partial) external {
		require(lockedDetals[token][msg.sender].lockedAmount == 0, "Cannot overwrite existing lock");
		IERC20(token).transferFrom(msg.sender, address(this), amount);
		details memory d;
		d.lockedAmount = amount;
		d.blockStart = block.number;
		d.blockStop = _days * 5760;
		d.partialUnlock = _partial;
		lockedDetals[token][msg.sender] = d;
	}

	function unLock(address token) external {
		details memory _details = lockedDetals[token][msg.sender];
		uint withdrawable = 0;
		if (block.number > _details.blockStart + _details.blockStop) {
			withdrawable = _details.lockedAmount;
		} 
        else withdrawable = _details.lockedAmount * (block.number - _details.blockStart) / _details.blockStop * _details.lockedAmount / _details.partialUnlock;

		require(withdrawable > 0, "Still locked!");
		IERC20(token).transfer(msg.sender, withdrawable - _details.unlockedAmount);

		_details.unlockedAmount = withdrawable;

		if (_details.unlockedAmount < _details.lockedAmount) lockedDetals[token][msg.sender] = _details;
		else delete lockedDetals[token][msg.sender];
	}
}
